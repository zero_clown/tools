package tool

import (
	"github.com/natefinch/lumberjack"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type LogConfig struct {
	Filename   string
	MaxSize    int
	MaxBackups int
	Compress   bool
}

func (data LogConfig) Logger() *zap.Logger {
	encoderConfig := zap.NewProductionEncoderConfig()
	encoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
	encoderConfig.EncodeLevel = zapcore.CapitalLevelEncoder
	encoder := zapcore.NewConsoleEncoder(encoderConfig)
	lumberJackLogger := &lumberjack.Logger{
		Filename:   data.Filename,   //日志文件的位置
		MaxSize:    data.MaxSize,    //在进行切割之前，日志文件的最大大小（以MB为单位）
		MaxBackups: data.MaxBackups, //保留旧文件的最大个数
		MaxAge:     7,               //保留旧文件的最大天数
		Compress:   data.Compress,   //是否压缩/归档旧文件
	}
	WriteSyncer := zapcore.AddSync(lumberJackLogger)
	core := zapcore.NewCore(encoder, WriteSyncer, zapcore.DebugLevel)
	logger := zap.New(core, zap.AddCaller())
	defer logger.Sync()
	return logger
}
